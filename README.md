# ansibletutorial

# Ansible

## What is Ansible

* Powerful IT automation and orchestration engine.
* simple enough to use
* powerful enough to automate complex application deployment

## What Ansible provides?
* simple automation language
  * used to describe end to end IT environment with playbook
* Automation Engine
  * used to run the playbook
* Ansible tower
  * Automation framework for control and manage ansible framework
  * Also known as awx

## Why ansible is popular?
* Simple
  * playbook are human and machine readable so coding experience required
  * Easily to understand the playbook and understand what is happening under the hood.
  * Tasks are always executed in order
* Lots of inbuilt ansible modules
  * Helps to manage infrastructure, network, storage and services easily
  * You can even run the playbook at multiple places irrespective of environment
* Agentless
  * It uses openssh and winRM protocol to automate
  * You donot need to install anything on the system you are automating on
  * No additional firewall ports to open
  * No need to setup a management infrastructure to run ansible playbook

## What you can do with Ansible?
* configuration manager
* Application deployment
* provisioning
* configuration delivery
* security and compliance
* orchrestration

## How Ansible works?
* To invoke actions on remote system, we need
  * inventory
    * list of hosts
  * playbooks and commands which describes the desired action
  * Ansible then take actions via transport
    * ssh for unix linux and network devices
    * winrm for windows system
    * APIs for cloud and another services
  * Ansible modules
    * It controls the things which are automating
    * They can do anything
      * controlling system resources
      * Installing/Removing packages
      * manuplating files
      * Making api calls to a service framework

## How to Install Ansible?
  * It is available everywhere
    * We recommend using os provided packages
    * For Fedora
      ```
      sudo dnf install ansible
      ```
  * Remember from where you want to run Ansible
    * that host we can call it control node
    * On this node Ansible needs to be Installed.

## Let's Dive into Ansible playground

### Check ansible is installed properly or not

* Run ansible --version command
```
[chkumar246@ironman ~]$ ansible --version
ansible 2.7.1
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/chkumar246/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3.7/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 3.7.1 (default, Nov  5 2018, 14:07:04) [GCC 8.2.1 20181011 (Red Hat 8.2.1-4)]

```

### Let's learn how to start automating

#### Playbooks
* written in ansible automation language which is based on yaml markup language
* It is a just few files built up to describe entire application as well as the environements they run it.

### Ansible variables
* It alters you to how playbooks run
* Used anywhere within playbook
* It can be inherited from inventory during runtime
* Explicitly set at runtime
* Read from a file
* Set as a result of a task

### Inventories
* A list of targents on which we want to run playbook from controller machine
* It can be stored in different ways
  * Static files in /etc/ansible/hosts
  * Dynamic list of servers pulled from a cloud provider generated via inventory script
* Define variables with in the inventory
  * which ssh key to use while logging to a server
```
---
[web]
web1.example.com
web2.example.com
[db]
db1.example.com
db2.example.com
```
* Let's create an inventory file
```
touch /etc/ansible/hosts
[testman]
127.0.0.1
```
* Make sure you copy your ssh to target machine from control machine
  * Generate ssh-key using
  ```
  ssh-keygen
  ```
  * copy the ssh key to target host
  ```
  ssh-copy-id -i <path to ssh generated key file id_rsa.pub> <username>@ip-address
  ```
  * Make sure your are able to ssh into each host
  
### Playbooks and tasks Tasks
* playbooks contains plays
* plays contains tasks
* Each tasks calls modules
* Tasks also run sequentily
* Handlers which are triggered by a task which can be runned by the end of the play

### Sample Playbook
```
[chkumar246@ironman Documents]$ cat playbook.yaml 
---
- name: Install and start apache
  hosts: web
  remote_user: justin
  become_method: sudo
  become_user: root
  vars:
    http_port: 80
    max_clients: 200

  tasks:
  - name: Install httpd
    yum: name=httpd state=latest
  - name: write apache config file
    template: src=src/httpd.j2 dest=/etc/httpd.j2
    notify:
    - restart apache
  - name: start httpd
    service: name=httpd state=running

  handlers:
  - name: restart apache
    service: name=httpd state=restarted

```

### Let's understand this playbook
* It has one play, 3 taska and a handler
* `---` indicates it is a yaml file
* `hosts` - which inventory to use
* `remote_user`: which user to use for remote connection
* `become_method`: to esclate privilages in this case we are using sudo
* `become_user`: you can do all the tasks without root user but if need you can switch to a specific user.
* `name`: directive used to display output of a task and readability of the playbook
* Each task has given a name followed by a module.
  * which tells ansible to configure the target system.
* What we are doing here:
  * package installation by yum
  * configuring apache service by template module
  * finally we are making sure apache service is started
  * If all the above steps in the above task happens successfully then handler will restart the service.

### Ansible module syntex
```
module: directive1=value1 directive2=value2
```

### We can do much more with playbooks
* You can alter indivisual tasks based on different conditions
* `with_items` - we can loop
* `failed_when` - Add a condition
* `changed_when`
* `until`
* `ignore_errors`

### Ansible Roles
* A special kind of playbooks that are fully self contained with tasks, variables, configuration templates and other supporting files that are needed to complete a complex orchrestation.

### Ansible Galaxy
* A place to ship and share user generated roles

## Automate with Ansible

### Running Ansible
* There are 3 ways to run ansible
  * Directly calling an ansible module
  ```
  ansible <inventory> -m 
  ```
  * Calling a playbook from command line
  ```
  ansible-playbook
  ```
  * By using ansible tower

### Running adhoc commands on target system
```
ansible ironman -a date
ansible ironman -m ping
```

### Running more complex task from cli
* Like making sure a package is updated
```
ansible ironman -s -m yum -a "name=openssl state=present"
```

### Let's run a playbook from ansible-playbook
```
ansible-playbook playbook.yaml
```
* If you run for the first time, the color will change to say the state
* But if we run twice it It will not do any thing, just verify it and say the status to green.
* If you make a change in file, then handler will be called once again re-running.

### Validate the playbook before running on the target system
```
ansible ironman -C -m yum -a "name=openssh state=present"
```
```
ansible-playbook -C playbook.yaml
```
It is the best way to validate the playbook before running on the target system. It just do a dry run on the system.

## Validating your playbook

Writting playbook is not hard, but once it become bigger. Syntex error are higher chances. Let's validate it.

For that we are going to use ansible-lint.

### ansible-lint
* It contains a collection of best practices for checking ansible playbook
* Installing it direct on the system
```
sudo dnf -y install ansible-lint
```
* Let's reuse it
```
ansible-lint <path to playbooks>
```

## Using playbook in gitlab ci
